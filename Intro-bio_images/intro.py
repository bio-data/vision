#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Displaying a simple image
=========================

Load and display an image
"""

import matplotlib.pyplot as plt
import skimage
import imageio  # type: ignore

# %% importing an image:
imagevar1 = skimage.io.imread("dna.jpg")
# or
imagevar2 = plt.imread("dna.jpg")
# or
imagevar3 = imageio.imread("dna.jpg")

assert (imagevar1 == imagevar2).all() == True
assert (imagevar1 == imagevar3).all() == True

# %% plotting an image:
skimage.io.imshow(imagevar1)
skimage.io.show()
# or
plt.imshow(imagevar1, cmap="Greys_r")
plt.show()

# %% Basic image histogram
plt.hist(imagevar1.ravel(), bins=range(256), fc="k", ec="k")

# %% Color
imagevar4 = skimage.io.imread("color.png")
