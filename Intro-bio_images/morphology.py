#!/usr/bin/python3
# -*- coding: utf-8 -*-

from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt

help(ndimage.generate_binary_structure)

struct_el = ndimage.generate_binary_structure(rank=2, connectivity=2)
struct_el = struct_el.astype(np.int64)

plt.imshow(struct_el, cmap="Greys_r")
plt.show()

# This is the default one anwyay,
# though I make it explicit.
struct_el = ndimage.generate_binary_structure(rank=2, connectivity=1)
struct_el = struct_el.astype(np.int64)

plt.imshow(struct_el, cmap="Greys_r")
plt.show()

big = np.zeros((7, 7), dtype=np.int64)
big[1:6, 2:5] = 1
big

plt.imshow(big, cmap="Greys_r")
plt.show()

small = np.zeros((5, 5), dtype=np.int64)
small[2, 2] = 1
small

plt.imshow(small, cmap="Greys_r")
plt.show()

help(ndimage.binary_erosion)
help(ndimage.binary_dilation)

big_erosion = ndimage.binary_erosion(input=big, structure=struct_el).astype(big.dtype)
plt.imshow(big_erosion, cmap="Greys_r")
plt.show()

big_dilation = ndimage.binary_dilation(input=big, structure=struct_el).astype(big.dtype)
plt.imshow(big_dilation, cmap="Greys_r")
plt.show()

small_erosion = ndimage.binary_erosion(input=small, structure=struct_el).astype(
    small.dtype
)
plt.imshow(small_erosion, cmap="Greys_r")
plt.show()

small_dilation = ndimage.binary_dilation(input=small, structure=struct_el).astype(
    small.dtype
)
plt.imshow(small_erosion, cmap="Greys_r")
plt.show()
